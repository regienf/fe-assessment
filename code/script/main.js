function callList(r) {
	$.getJSON("api/facts/"+r+".json", function( data ) {
		var dd = data.all;
		$.each( dd, function(i, v ) {
			var t = dd[i].text;
			if(typeof dd[i].user === "undefined") {
				var a = "";
			} else {
				var a = dd[i].user.name.first + " " + dd[i].user.name.last;
			}
			
			$('#'+r).find('.pop-content').append("<div class=\"pop-item\"><p>"+t+"</p><span>"+a+"</span></div>");
		});
	});	
}

$(document).ready(function() {
	$('.btn-cta').bind('click', function() {
		var n = $(this).attr('ref');
		callList(n);
	});
})